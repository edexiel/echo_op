using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonar : MonoBehaviour
{
    [SerializeField] Material material = default;
    [SerializeField] float minRadius = 1;


    private bool isWaving;

    Coroutine wave;

    public void DoSonarWave(Vector3 position, float speed, float finalRadius)
    {
        if (isWaving) return;

        if (wave != null) StopCoroutine(wave);
        wave = StartCoroutine(Wave(position, speed, finalRadius));


    }

    private IEnumerator Wave(Vector3 position, float speed, float finalRadius)
    {
        float radius = minRadius;
        isWaving = true;
        material.SetVector("Sonar_Middle", position);

        while(radius < finalRadius)
        {
            Debug.Log(material.GetFloat("Sonar_Radius"));

            radius += Time.deltaTime * speed;
            material.SetFloat("Sonar_Radius", radius);
            yield return null;
        }

        isWaving = false;
        yield break;
    }
}